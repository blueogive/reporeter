#!/bin/sh

cat slides/*.md | pandoc --toc --variable=date:"Last Updated: `date +"%A %F %H:%M %Z"`" -f markdown -t html5 -s --css=style/markdown2.css --mathjax=http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML-full -o ${1}.html
