#!/usr/bin/env Rscript

findrmd <- function(path='.') {
    # Return the name of the Rmarkdown file in the current directory
    # matching the name of the current directory. This conforms to the
    # default name of the master/parent Rmarkdown file if the folder
    # were created using rmarkdown::draft.
    currdir <- tail(strsplit(getwd(), '/', fixed=TRUE)[[1]], n=1)
    rmdf <- list.files(path, pattern=paste0(currdir, '.Rmd'))
    if (length(rmdf)) {
       retval <- rmdf[[1]]
    } else {
       retval <- list.files(path, pattern='*.Rmd', recursive = TRUE)[[1]]
    }
    retval
}

rmarkdown::render(findrmd(), 'all', clean=TRUE)

sessionInfo()
