# Synchronized Audio

You can place appropriately named Ogg Vorbis audio files this folder
and the tracks will be automatically played back in time with the
slides, which will also automatically advance. Sound complicated? It's
actually remarkably elegant and simple. If you have questions about
this feature, ask [Mark Coggeshll](mailto:mark.coggeshall@csosa.gov)
or [David Fink](mailto:david.fink@csosa.gov).
