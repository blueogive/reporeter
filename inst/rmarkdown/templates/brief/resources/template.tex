\documentclass[$for(classoption)$$classoption$$sep$,$endfor$]{tufte-handout}

\hypersetup{colorlinks,
    citecolor=$if(citecolor)$$citecolor$$else$DarkGreen$endif$,
    urlcolor=$if(urlcolor)$$urlcolor$$else$DarkGreen$endif$,
    linkcolor=$if(linkcolor)$$linkcolor$$else$DarkBlue$endif$,
    bookmarks=true,
    hyperfigures=true,
    bookmarksopen=true,
    bookmarksopenlevel=3,
    breaklinks=true,
    bookmarksnumbered=true,
    pdfpagemode=UseOutlines,
    unicode,
    pdftitle={$title$},
    pdfauthor={$for(author)$$author.name$$sep$ and $endfor$}
}

% ams
\usepackage{amssymb,amsmath}

\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript

\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else
  \usepackage{fontspec}
\fi

% fonts
\setmainfont[Ligatures={Common},Numbers={OldStyle},SmallCapsFont={Sabon RomanSC}]{Pali}
\setsansfont[Mapping=tex-text]{TeX Gyre Heros}
\setmonofont[Scale=0.88]{Source Code Pro}
\renewcommand\allcapsspacing[1]{{\addfontfeature{LetterSpace=20.0}#1}}
\renewcommand\smallcapsspacing[1]{{\addfontfeature{LetterSpace=5.0}#1}}
\renewcommand\textsc[1]{\smallcapsspacing{\textsmallcaps{#1}}}
\renewcommand\smallcaps[1]{\smallcapsspacing{\scshape\MakeTextLowercase{#1}}}
%%
% If they're installed, use Bergamo and Chantilly from www.fontsite.com.
% They're clones of Bembo and Gill Sans, respectively.
%\IfFileExists{bergamo.sty}{\usepackage[osf]{bergamo}}{}% Bembo
%\IfFileExists{chantill.sty}{\usepackage{chantill}}{}% Gill Sans

% graphix
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}

% url
\usepackage{url}

% hyperref
\usepackage{hyperref}

% units.
\usepackage{units}

% pandoc syntax highlighting
$if(highlighting-macros)$
$highlighting-macros$
$endif$

% longtable
\usepackage{longtable,booktabs}
\usepackage[flushleft]{threeparttable} % support for table notes
\usepackage{dcolumn}  % decimal-aligned columns in tables

% landscape mode
\usepackage{lscape}
\usepackage{rotating}
\usepackage{afterpage}
\usepackage{varioref} % \vref for more thoughtful xreferences
% a custom command for named crossrefs with page notes
\newcommand{\namevpref}[1]{\emph{\nameref{#1}} \vpageref{#1}}

% multiplecol
\usepackage{multicol}

% lipsum
\usepackage{lipsum}

% strikeout
\usepackage[normalem]{ulem}

% morefloats
\usepackage{morefloats}

\usepackage{fancyvrb} % extended verbatim environments
\fvset{fontsize=\normalsize,
  } % default font size for Verbatim environments

% support for framed paragraphs
\usepackage{framed}

\usepackage{mathtools} % bug fixes, improvements to amsmath
% note that the tufte-handout documentclass is incompatible with
% the way the acronym package outputs its list of acronyms.
% Hence, the use of the nolist package option here.
\usepackage[smaller,printonlyused,nolist]{acronym}
\usepackage{import} % to input my cross-projects acronyms file
\usepackage{textcomp}

% multi-language settings
\ifxetex
  \usepackage{polyglossia}
  \setmainlanguage[variant=american]{english}
\else
  \usepackage[shorthands=off,american]{babel}
\fi

% Reference management declarations/options
\usepackage{csquotes} % improved commands for typesetting quotations
\usepackage[
  style=verbose,
  autocite=footnote,
  backend=biber,
  doi=true,
  url=true
]{biblatex}
\DeclareLanguageMapping{american}{american-apa}
% Encourage better line breaking of URLs in citations.
\setcounter{biburllcpenalty}{9000}
\setcounter{biburlucpenalty}{9000}
\setcounter{biburlnumpenalty}{9000}
% Reference bibliography database(s)
$for(biblio-files)$
\addbibresource{$biblio-files$}
$endfor$
\IfFileExists{knitcitations.bib}{\addbibresource{knitcitations.bib}}


% Standardize command font styles and environments
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment

%%
% Prints argument within hanging parentheses (i.e., parentheses that take
% up no horizontal space). Useful in tabular environments.
\newcommand{\hangp}[1]{\makebox[0pt][r]{(}#1\makebox[0pt][l]{)}}

%%
% Prints an asterisk that takes up no horizontal space.
% Useful in tabular environments.
\newcommand{\hangstar}{\makebox[0pt][l]{*}}

%%
% Prints a trailing space in a smart way.
\usepackage{xspace}
%new commands to get spacing correct around full stops
\newcommand{\eg}{e.g.,\xspace}
\newcommand{\ie}{i.e.,\xspace}
\newcommand{\etc}{etc.\xspace}
\newcommand{\etal}{et al.\xspace}
%length mods the get attractive spacing around floats and to prevent
%floats from stacking up in the queue.
\setlength{\abovecaptionskip}{1mm}
\setlength{\belowcaptionskip}{1mm}
\setlength{\intextsep}{1mm}
\setlength{\textfloatsep}{1mm}
\renewcommand\floatpagefraction{.7}
\renewcommand\topfraction{.9}
\renewcommand\bottomfraction{.8}
\renewcommand\textfraction{0.1}
\renewcommand{\dbltopfraction}{0.9}	% fit big float above 2-col. text
\renewcommand{\dblfloatpagefraction}{0.7}
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}
\setcounter{dbltopnumber}{2}

$if(numbersections)$
\setcounter{secnumdepth}{5}
$else$
\setcounter{secnumdepth}{0}
$endif$

% Inserts a blank page
\newcommand{\blankpage}{\newpage\hbox{}\thispagestyle{empty}\newpage}

% Define a command used by Pandoc >=1.14
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

% title / author / date
$if(title)$
\title{$title$}
$endif$
$if(author)$
\author{$for(author)$$author.name$$sep$ and $endfor$}
$endif$

\publisher{
  $if(principal)$
  \footnotesize

  $for(principal)$
    \hspace{18pt}\quad
    $principal.name$ \\
    \hspace{18pt}\quad
    $principal.title$ \\
    \hspace{18pt}\quad
    $principal.office$ \\
    \medskip
  $endfor$
  $endif$
}

$if(date)$
\date{$date$}
$endif$

$if(watermark)$
\usepackage[printwatermark]{xwatermark}
\newwatermark[allpages,color=red!33,angle=45,scale=3,xpos=0,ypos=0]{$watermark$}
$endif$

$for(header-includes)$
$header-includes$
$endfor$

\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
%
\else
% work around bug with interaction between a package and luatex/xetex, without using the NOLS option above
  \renewcommand{\allcapsspacing}[1]{{\addfontfeature{LetterSpace=20.0}#1}}
  \renewcommand{\smallcapsspacing}[1]{{\addfontfeature{LetterSpace=5.0}#1}}
  \renewcommand{\textsc}[1]{\smallcapsspacing{\textsmallcaps{#1}}}
  \renewcommand{\smallcaps}[1]{\smallcapsspacing{\scshape\MakeTextLowercase{#1}}}
\fi

\begin{document}

$if(title)$
\maketitle
$endif$

\begin{marginfigure}[-0.4in]
\centering
$if(logo)$
% For reasons unknown, making the logo a hyperlink using the
% commented statement below, throws LaTeX into a loop causing
% it to exceed its memory allocation. I suspect a LaTeX package
% bug. --mbc, 2017-03-20
% \href{$orgurl$}{
  \includegraphics[width=.7\marginparwidth]{$logo$}
%  }
$endif$
\end{marginfigure}
\marginpar{\thanklesspublisher}

$if(abstract)$
\begin{abstract}
\noindent $abstract$
\end{abstract}
$endif$

$for(include-before)$
$include-before$
$endfor$

$body$

$if(acronymsfile)$
\import*{$pathtoacronyms$}{$acronymsfile$}
$endif$

$for(include-after)$
$include-after$
$endfor$

\end{document}
