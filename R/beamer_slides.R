
#' Beamer slide deck (PDF)
#'
#' Template for creating a PDF slide deck using the LaTeX
#' beamer package
#'
#' @inheritParams rmarkdown::beamer_presentation
#'
#' @export
beamer_slides <- function(toc = TRUE,
                          toc_depth = 3,
                          number_sections = TRUE,
                          fig_width = 6,
                          fig_height = 5,
                          fig_crop = TRUE,
                          highlight = "default",
                          keep_tex = FALSE,
                          latex_engine = "xelatex",
                          includes = NULL,
                          pandoc_args = NULL,
                          slide_level = slide_level,
                          theme = theme,
                          colortheme = colortheme,
                          fonttheme = fonttheme,
                          fig_caption = fig_caption,
                          ...
                          ) {

  # resolve default highlight
  if (identical(highlight, "default"))
    highlight <- "pygments"

  # get the template
  template <- find_resource('beamer_slides', 'template.tex')

  # call the base pdf_document format with the appropriate options
  format <- rmarkdown::beamer_presentation(
                                    fig_width = fig_width,
                                    fig_height = fig_height,
                                    fig_crop = fig_crop,
                                    highlight = highlight,
                                    keep_tex = keep_tex,
                                    latex_engine = latex_engine,
                                    includes = includes,
                                    pandoc_args = pandoc_args,
                                    toc = toc,
                                    template = template,
                                    slide_level = slide_level,
                                    theme = theme,
                                    colortheme = colortheme,
                                    fonttheme = fonttheme,
                                    fig_caption = fig_caption,
                                    ...
                                    )


  # create knitr options (ensure opts and hooks are non-null)
  knitr_options <- rmarkdown::knitr_options_pdf(fig_width, fig_height, fig_crop)
  if (is.null(knitr_options$opts_knit))
    knitr_options$opts_knit <- list()
  if (is.null(knitr_options$knit_hooks))
    knitr_options$knit_hooks <- list()

  # set options
  knitr_options$opts_chunk$tidy <- TRUE
  knitr_options$opts_knit$width <- 45

  # set hooks for special plot output
  ## knitr_options$knit_hooks$plot <- function(x, options) {

  ##   # determine caption (if any)
  ##   caption <- ifelse(is.null(options$fig.cap),
  ##                     "",
  ##                     paste("\\caption{", options$fig.cap, "}\n", sep = ""))

  ##   # determine figure type
  ##   if (isTRUE(options$fig.margin))
  ##     figtype <- "marginfigure"
  ##   else if (isTRUE(options$fig.fullwidth))
  ##     figtype <- "figure*"
  ##   else
  ##     figtype <- "figure"

  ##   # return the latex
  ##   paste(sprintf('\\begin{%s}\n \\includegraphics{%s}\n%s\\end{%s}\n',
  ##                 figtype, x, caption, figtype))
  ## }

  # override the knitr settings of the base format and return the format
  format$knitr <- knitr_options
  format
}
