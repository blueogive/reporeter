# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
FROM registry.dc0633ubu01.csosa.gov/ore/templates/mro-docker/mro-docker:v3.5.1

USER root

COPY reporeter.zip reporeter.zip

RUN unzip reporeter.zip \
	&& Rscript -e "withr::with_libpaths(new = .libPaths()[2], devtools::install('.'))" \
	&& rm reporeter.zip

ARG VCS_URL=${VCS_URL}
ARG VCS_REF=${VCS_REF}
ARG BUILD_DATE=${BUILD_DATE}

# Add image metadata
LABEL org.label-schema.license="https://mran.microsoft.com/faq/#licensing" \
    org.label-schema.vendor="Court Services and Offender Supervision Agency, Dockerfile provided by Mark Coggeshall" \
	org.label-schema.name="MRO with reporeter package" \
	org.label-schema.description="Microsoft R Open (MRO) with the CSOSA reporeter package installed." \
	org.label-schema.vcs-url=${VCS_URL} \
	org.label-schema.vcs-ref=${VCS_REF} \
	org.label-schema.build-date=${BUILD_DATE} \
	maintainer="Mark Coggeshall <mark.coggeshall@csosa.gov>"

ENTRYPOINT ["/bin/bash"]
