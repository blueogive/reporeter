.PHONY : zippkg docker-build docker-tag docker-push

PKGNAME := $(notdir $(CURDIR))
TAGBASE := registry.dc0633ubu01.csosa.gov/ore/templates/mro-docker/
TAGNAME := mro-$(PKGNAME)

zippkg:
	@zip -r $(PKGNAME).zip . -x Makefile -x Dockerfile -x .git\* -x .vscode\*

docker-build: Dockerfile zippkg
	@docker build \
	--build-arg VCS_URL=$(VCS_URL) \
	--build-arg VCS_REF=$(VCS_REF) \
	--build-arg BUILD_DATE=$(BUILD_DATE) . \
	-t $(TAGBASE)$(TAGNAME)

docker-tag: docker-build
	@docker tag $(TAGBASE)$(TAGNAME) $(TAGBASE)$(TAGNAME):v3.5.1
	@docker tag $(TAGBASE)$(TAGNAME) $(TAGBASE)$(TAGNAME):latest

docker-push: docker-tag
	@docker push $(TAGBASE)$(TAGNAME):v3.5.1
	@docker push $(TAGBASE)$(TAGNAME):latest
