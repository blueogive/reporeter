# Overview

The **reporeter** (pronounced like 'reporter') package includes a set
of [R Markdown](http://rmarkdown.rstudio.com) templates that enable
authoring of memoranda, reports, and slide decks with support for
organizational branding.
Available templates include:

- *Brief*: a report without chapters or title page in the
  style of Edward R. Tufte and Richard Feynman featuring a narrow main
  column for text and wide margin for marginalia, including small
  tables, figures, and sidenotes;
- *Report*: a book-length PDF report with title page, abstract,
  acknowledgements, table of contents, lists of
  tables/figures/acronyms, executive summary, multiple
  chapters/appendices and convenient management of acronyms,
  citations, and cross-references;
- *Memorandum*: a PDF memorandum with support for convenient management
  of acronyms, references, and exhibits; and
- *Fact Sheet*: a 1-2 page HTML file designed to convey research
  findings in a brief format. By default, it features a narrow left
  column for stating key findings and implications and a wider main
  column for exhibits. With minimal effort, this default layout can be
  transformed to suit other purposes. For example, a two-column layout
  at the top for text with one wide column below for exhibits. One can
  produce PDF from the HTML output by using the print-to-PDF feature
  of browsers like Firefox and Chrome. By default, figures are written
  to SVG format, but this is easily changed to PNG where desired.
- *Reveal Slides*: an HTML slide deck combining Markdown content
  with a display engine provided by
  [Reveal.js](https://github.com/hakimel/reveal.js).
- *Beamer Slides*: a PDF slide deck using the LaTeX Beamer package.

Under the hood, a combination of HTML (for the fact sheet and Reveal
slides) and LaTeX templates (for everything else) are used to ensure
that documents conform precisely to submission standards. At the same
time, composition and formatting can be done using lightweight
[Markdown](http://rmarkdown.rstudio.com/authoring_basics.html) syntax,
and `R` code and its output can be seamlessly included using
[knitr](http://yihui.name/knitr/).

# Using **reporeter**

## Within RStudio

1) From the menu, click `File` -> `New File` -> `R Markdown` to launch the dialog shown below:

![New R Markdown](http://rmarkdown.rstudio.com/images/new_r_markdown.png)

2) From the left side of the dialog, click `From Template`. Then,
choose from among the `reporeter` templates on the right.

3) None of the templates is empty. The content of each is designed to
serve as an example of how to use the features each has to offer. To
build your document, click the `knit` buttom from the toolbar.

## Within R

1) Call the `rmarkdown::draft` function to instantiate your document
from the desired template. Note that each template creates not just
one file, but a new directory with one or more subdirectories of
supporting files:

```S
rmarkdown::draft("MyFactsheet.Rmd", template = "factsheet", package = "reporeter", edit = FALSE)
```

2) Call the `rmarkdown::render` function to build your document:

```S
rmarkdown::render("MyFactsheet/MyFactsheet.Rmd", "all")
```

If the document fails to build, it may be useful to prevent `R` from
tidying up the intermediate files it creates during the build. The
`clean` argument controls this behavior:

```S
rmarkdown::render("MyFactsheet/MyFactsheet.Rmd", "all", clean = FALSE)
```

## The `reveal_slides` Template

With one exception, all of the templates are self-contained, meaning
that you can instantiate the document, build, and view it immediately.
That exception is the `reveal_slides` template, which expects you to
create a new subdirectory named `reveal` within the directory created
when the document was instantiated from the template. The `reveal`
subdirectory must contain Reveal.js itself, which is a rather large
collection of files in itself. Fortunately, satisfying this dependency
is fairly simple.

If you plan to display your slides from `DC0633UBU02`, you can simply
create a symbolic link pointing to the version of Reveal.js already on
the file system:

```Bash
cd /path/to/MyFactsheet
ln -s /home/tools/reveal.js reveal
```

Otherwise, you can clone my Reveal.js repo from
[Bitbucket](https://bitbucket.org) using HTTPS:

```Bash
cd /path/to/MyFactsheet
git clone https://<your_username>@bitbucket.org/blueogive/reveal.js.git reveal
```

or SSH:

```Bash
cd /path/to/MyFactsheet
git clone git@bitbucket.org:blueogive/reveal.js.git reveal
```

# Installation

Steps 1–3 require administrative privileges, and
administrative privileges are desirable for Step 5, as well.

1) If necessary, install the latest versions of
[R](http://cran.r-project.org) and
[RStudio](http://www.rstudio.com/products/rstudio/download/).

2) Install [pandoc](http://johnmacfarlane.net/pandoc/) using the
[instructions for your platform](https://github.com/rstudio/rmarkdown/blob/master/PANDOC.md).

3) Install the **rmarkdown** package:

```S
devtools::install_github("rstudio/rmarkdown")
```

4) Create a local clone of the the **reporeter** repository:

```Bash
git clone https://blueogive@bitbucket.org/blueogive/reporeter.git
```

5) Install the **reporeter** package:

Launch `R`. If you have administrative privileges on the host, launch R
as an administator to install/upgrade **reporeter** for all users.

If `R` is running as an administrator, first call `.libPaths()`:

```S
.libPaths()
```

If this call returns only one path, you can install `reporter` using
`devtools` alone:

```S
devtools::install("path/to/reporeter")
```

Otherwise, if `.libPaths()` returns two or more paths and the target path you want is not the first, you need to use the `withr` package to hit the
desired target:

```S
withr::with_libpaths(new = .libPaths()[2], devtools::install("path/to/reporeter"))
```

This example assumes your target installation path is the *second* path
returned by `.libPaths()`. If you want the *third* path, replace:

```S
.libPaths()[2]
```

with

```S
.libPaths()[3]
```

# To Do

* In early November 2017, the Agency updated its design elements and
  established a more formal color palette and font guidance. The `reporeter`
  package needs to be updated to reflect this design guidance:
    * Blue: #051D49
    * Gray: #404040
    * Red: #A32A29
    * Yellow: #C4A11C
    * Font (sans-serif): Oswald-Regular

The font is available from the Google Fonts API:

```html
<style>
@import url('https://fonts.googleapis.com/css?family=Oswald');
</style>
```
